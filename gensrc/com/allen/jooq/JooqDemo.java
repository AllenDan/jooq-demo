/*
 * This file is generated by jOOQ.
*/
package com.allen.jooq;


import com.allen.jooq.tables.Group;
import com.allen.jooq.tables.GroupAuthority;
import com.allen.jooq.tables.GroupMember;
import com.allen.jooq.tables.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JooqDemo extends SchemaImpl {

    private static final long serialVersionUID = 863836768;

    /**
     * The reference instance of <code>jooq_demo</code>
     */
    public static final JooqDemo JOOQ_DEMO = new JooqDemo();

    /**
     * The table <code>jooq_demo.group</code>.
     */
    public final Group GROUP = com.allen.jooq.tables.Group.GROUP;

    /**
     * The table <code>jooq_demo.group_authority</code>.
     */
    public final GroupAuthority GROUP_AUTHORITY = com.allen.jooq.tables.GroupAuthority.GROUP_AUTHORITY;

    /**
     * The table <code>jooq_demo.group_member</code>.
     */
    public final GroupMember GROUP_MEMBER = com.allen.jooq.tables.GroupMember.GROUP_MEMBER;

    /**
     * The table <code>jooq_demo.user</code>.
     */
    public final User USER = com.allen.jooq.tables.User.USER;

    /**
     * No further instances allowed
     */
    private JooqDemo() {
        super("jooq_demo", null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Group.GROUP,
            GroupAuthority.GROUP_AUTHORITY,
            GroupMember.GROUP_MEMBER,
            User.USER);
    }
}
