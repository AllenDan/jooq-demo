package com.allen.core.service;


import com.allen.core.model.IdentifiableDTO;
import com.allen.core.repository.AbstractCRUDRepository;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;

import java.util.List;
import java.util.Map;
import java.util.Optional;


@Slf4j
public abstract class AbstractCRUDService<ID, P extends IdentifiableDTO<ID>> {

    protected final DSLContext dslContext;

    public AbstractCRUDRepository<?, ID, P> getRepository() {
        return repository;
    }

    protected final AbstractCRUDRepository<?, ID, P> repository;


    public AbstractCRUDService(DSLContext dslContext, AbstractCRUDRepository<?,ID, P> abstractCRUDRepository) {
        this.dslContext = dslContext;
        this.repository = abstractCRUDRepository;
    }

    public List<P> getPage(Integer page, Integer pageSize, String sortField, String sortOrder, Map<String, Object> filterQuery) {
         return this.repository.getPage(page, pageSize, sortField, sortOrder, filterQuery);
    }

    public Optional<P> findOptionalById(ID id) {
        return this.repository.findOptionalById(id);
    }

    public P update(ID id, P pojo) {
        return this.repository.update(id, pojo);
    }

    public P create(P pojo) {
        return this.repository.create(pojo);

    }

    public void delete(ID id) {
        this.repository.delete(id);
    }

    public int count(Map<String, Object> filterQuery) {
        return this.repository.count(filterQuery);
    }

    public P findByQuery(Map<String, Object> filterQuery) {

        return this.repository.findByQuery(filterQuery);

    }


}
