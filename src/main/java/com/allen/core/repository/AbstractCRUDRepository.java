package com.allen.core.repository;

import com.allen.core.model.IdentifiableDTO;
import com.allen.core.model.Page;

import com.google.common.collect.Lists;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * author: Allen
 */

/**
 *  CRUD
 * @param <R>
 * @param <ID>
 * @param <P>
 */
public abstract class AbstractCRUDRepository<R extends UpdatableRecord, ID, P extends IdentifiableDTO<ID>> {

    protected final DSLContext dslContext;
    private final Table<R> table;
    private final Field<ID> idField;
    private final Class<P> pojoClass;



    public AbstractCRUDRepository(DSLContext dslContext, Table<R> table, Field<ID> idField, Class<P> pojoClass) {
        this.dslContext = dslContext;
        this.table = table;
        this.pojoClass = pojoClass;
        this.idField = idField;
    }

    public List<P> getPage(Integer page, Integer pageSize, String sortField, String sortOrder, Map<String, Object> filterQuery) {
        String sortFieldName = sortField.replaceAll("([A-Z])", "_$1").toLowerCase();
        return dslContext.selectFrom(table)
                .where(filter(filterQuery != null ? filterQuery : new HashMap<>()))
                .orderBy(table.field(sortFieldName).sort(SortOrder.valueOf(sortOrder)))
                .limit(pageSize)
                .offset((page - 1) * pageSize)
                .fetchInto(pojoClass);
    }

    public int count(Map<String, Object> filterQuery) {

        return dslContext.selectCount().from(table).where(filter(filterQuery != null ? filterQuery : new HashMap<>()))
                .fetchOne(0, int.class);

    }

    public P getById(ID id) {
        R record = getRecordById(id);
        return record == null ? null : record.into(pojoClass);
    }

    public Optional<P> getByIdToOptional(ID id) {
        R record = getRecordById(id);
        return record == null ? Optional.ofNullable(null) : Optional.ofNullable(record.into(pojoClass));
    }

    public Optional<P> findOptionalById(ID id) {
        return findOptional(idField, id);
    }

    protected  <Z> Optional<P> findOptional(Field<Z> field, Z value) {
        return dslContext.selectFrom(table).where(field.eq(value)).fetchOptionalInto(pojoClass);
    }

    public P update(ID id, P pojo) {
        R record = getRecordById(id);

        record.from(pojo);
        record.store();
        return record.into(pojoClass);
    }

    public P findByQuery(Map<String, Object> filterQuery) {
        return dslContext.selectFrom(table)
                .where(filter(filterQuery != null ? filterQuery : new HashMap<>()))
                .fetchOneInto(pojoClass);
    }

    public P create(P pojo) {

        R record = newRecord(pojo);

        record.store();
        return record.into(pojoClass);
    }

    public void delete(ID id) {
        dslContext.delete(table).where(idField.eq(id)).execute();
    }

    protected Condition filter(Map<String, Object> filterQuery) {
        return null;
    }

    protected R getRecordById(ID id) {
        return dslContext.selectFrom(table)
                .where(idField.eq(id))
                .fetchOne();
    }

    protected R newRecord(Object obj) {

        return dslContext.newRecord(table, obj);
    }

    public P getOneById(ID id) {
        return dslContext.selectFrom(table)
                .where(idField.eq(id))
                .fetchOneInto(pojoClass);
    }

    public P getOne(Condition condition) {
        return dslContext.selectFrom(table)
                .where(condition)
                .limit(1)
                .fetchOneInto(pojoClass);
    }

    public List<P> list(Condition condition) {
        return dslContext.selectFrom(table)
                .where(condition)
                .orderBy(idField.asc())
                .fetchInto(pojoClass);
    }

    public List<P> list(Condition condition, SortField sortField) {
        return dslContext.selectFrom(table)
                .where(condition)
                .orderBy(sortField)
                .fetchInto(pojoClass);
    }

    public List<P> list(Condition condition, String sortField, String sortOrder) {
        return dslContext.selectFrom(table)
                .where(condition)
                .orderBy(table.field(sortField).sort(SortOrder.valueOf(sortOrder)))
                .fetchInto(pojoClass);
    }

    public List<P> list(Condition condition, List<SortField> sortFields) {
        SortField[] sortFieldArray = new SortField[sortFields.size()];
        sortFields.toArray(sortFieldArray);

        return dslContext.selectFrom(table)
                .where(condition)
                .orderBy(sortFieldArray)
                .fetchInto(pojoClass);
    }

    public List<P> list(Integer pageNum, Integer pageSize, Condition condition) {
        return list(pageNum, pageSize, condition, null);
    }

    public List<P> list(Integer pageNum, Integer pageSize, Condition condition, List<SortField> sortFields) {
        SortField[] sortFieldArray = new SortField[sortFields.size()];
        sortFields.toArray(sortFieldArray);

        return dslContext.selectFrom(table)
                .where(condition)
                .orderBy(sortFieldArray)
                .limit(pageSize)
                .offset((pageNum - 1) * pageSize)
                .fetchInto(pojoClass);
    }

    public Page<P> page(Integer pageNum, Integer pageSize, Condition condition) {
        return page(pageNum, pageSize, condition, idField.asc());
    }

    public Page<P> page(Integer pageNum, Integer pageSize, Condition condition, SortField sortField) {
        List<SortField> sortFields = Lists.newArrayList();
        sortFields.add(sortField);
        return page(pageNum, pageSize, condition, sortFields);
    }

    public Page<P> page(Integer pageNum, Integer pageSize, Condition condition, String sortField, String sortOrder) {
        List<SortField> sortFields = Lists.newArrayListWithCapacity(1);
        sortFields.add(table.field(sortField).sort(SortOrder.valueOf(sortOrder)));
        return page(pageNum, pageSize, condition, sortFields);
    }

    public Page<P> page(Integer pageNum, Integer pageSize, Condition condition, SortField sortField,
                        List<GroupField> groupFields) {
        List<SortField> sortFields = Lists.newArrayList();
        sortFields.add(sortField);
        return page(pageNum, pageSize, condition, sortFields, groupFields);
    }
    public Page<P> page(Integer pageNum, Integer pageSize, Condition condition, String sortField, String sortOrder,
                        List<GroupField> groupFields) {
        List<SortField> sortFields = Lists.newArrayListWithCapacity(1);
        sortFields.add(table.field(sortField).sort(SortOrder.valueOf(sortOrder)));
        return page(pageNum, pageSize, condition, sortFields, groupFields);
    }

    public Page<P> page(Integer pageNum, Integer pageSize, Condition condition, List<SortField> sortFields) {
        SortField[] sortFieldArray = new SortField[sortFields.size()];
        sortFields.toArray(sortFieldArray);

        int totalRow = getTotalRow(condition);

        if (totalRow == 0) {
            return new Page<>(Lists.newArrayListWithCapacity(0), pageNum, pageSize, 0, 0);
        } else {
            int totalPage = getTotalPage(totalRow, pageSize);

            if (pageNum > totalPage) {
                return new Page<>(Lists.newArrayListWithCapacity(0), pageNum, pageSize, totalPage, totalRow);
            } else {
                List<P> list = dslContext
                        .selectFrom(table)
                        .where(condition)
                        .orderBy(sortFieldArray)
                        .limit(pageSize)
                        .offset((pageNum - 1) * pageSize)
                        .fetchInto(pojoClass);
                return new Page<>(list, pageNum, pageSize, totalPage, totalRow);
            }
        }
    }

    public Page<P> page(Integer pageNum, Integer pageSize, Condition condition, List<SortField> sortFields,
                        List<GroupField> groupFields) {
        SortField[] sortFieldArray = new SortField[sortFields.size()];
        sortFields.toArray(sortFieldArray);

        int totalRow = getTotalRow(condition);

        if (totalRow == 0) {
            return new Page<>(Lists.newArrayListWithCapacity(0), pageNum, pageSize, 0, 0);
        } else {
            int totalPage = getTotalPage(totalRow, pageSize);

            if (pageNum > totalPage) {
                return new Page<>(Lists.newArrayListWithCapacity(0), pageNum, pageSize, totalPage, totalRow);
            } else {
                List<P> list = dslContext
                        .selectFrom(table)
                        .where(condition)
                        .groupBy(groupFields)
                        .orderBy(sortFieldArray)
                        .limit(pageSize)
                        .offset((pageNum - 1) * pageSize)
                        .fetchInto(pojoClass);
                return new Page<>(list, pageNum, pageSize, totalPage, totalRow);
            }
        }
    }

    public int getTotalRow(Condition condition) {
        Result<Record1<Integer>> result = dslContext.selectCount()
                .from(table)
                .where(condition).fetch();
        return result == null || result.isEmpty() ? 0 : result.get(0).value1();
    }

    public int getTotalPage(int totalRow, int pageSize) {
        int totalPage = totalRow / pageSize;
        if (totalRow % pageSize != 0L) {
            ++totalPage;
        }
        return totalPage;
    }

    public static Field<String> dateFormat(Field<Timestamp> field, String format) {
        return DSL.field("date_format({0}, {1})", SQLDataType.VARCHAR, field, DSL.inline(format));
    }
}

