package com.allen.core.rest;


import com.allen.core.model.IdentifiableDTO;
import com.allen.core.repository.AbstractCRUDRepository;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;



public abstract class AbstractCRUDRestController<ID, Resource extends IdentifiableDTO<ID>> extends AbstractRestController<ID, Resource> {

    public AbstractCRUDRestController(AbstractCRUDRepository<?, ID, Resource> repository) {
        super(repository);
    }

    @ApiOperation(value = "获取 Resource 列表接口", notes = "分页获取Resource 列表接口")
    @ApiImplicitParams({})
    @Override
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public List<Resource> page(@RequestParam(value = "page", defaultValue = "1") Integer page,
                               @RequestParam(value = "size", defaultValue = "30") Integer pageSize,
                               @RequestParam(value = "sort_dir", defaultValue = "DESC") String sortOrder,
                               @RequestParam(value = "sort_field", defaultValue = "id") String sortField,
                               @RequestParam(value = "filters", required = false) String query) {
        return super.page(page, pageSize, sortOrder, sortField, query);
    }

    @ApiOperation(value = "获取单个Resource 接口", notes = "Resource 接口")
    @ApiImplicitParams({})
    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Resource get(@PathVariable("id") ID id, @RequestHeader(value="Authorization", required = false) String jwtToken) {
        return super.get(id, jwtToken);
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> create(@RequestBody Resource dto, @RequestHeader(value="Authorization", required = false) String jwtToken) {
        return super.create(dto,jwtToken);
    }

    @ApiOperation(value = "Resource 更新接口", notes = "Resource 更新接口")
    @ApiImplicitParams({})
    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Void> update(@PathVariable ID id, @RequestBody Resource dto, @RequestHeader(value="Authorization", required = false) String jwtToken) {
        return super.update(id, dto, jwtToken);
    }

    @ApiOperation(value = "Resource 删除接口", notes = "Resource 删除接口")
    @ApiImplicitParams({})
    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") ID id, @RequestHeader(value="Authorization", required = false) String jwtToken) {
        return super.delete(id, jwtToken);
    }
}
