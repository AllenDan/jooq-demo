package com.allen.core.rest;


import com.allen.core.model.IdentifiableDTO;
import com.allen.core.service.AbstractCRUDService;
import org.springframework.boot.json.BasicJsonParser;
import org.springframework.boot.json.JsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.util.List;
import java.util.Map;


abstract class AbstractServiceRestController <ID, Resource extends IdentifiableDTO<ID>>{
    protected  final AbstractCRUDService<ID, Resource> service;
    JsonParser jsonParser;

    AbstractServiceRestController(AbstractCRUDService< ID, Resource> service) {
        this.service = service;
        this.jsonParser = new BasicJsonParser();
    }

    protected List<Resource> page(Integer page, Integer pageSize, String sortOrder, String sortField, String queryString) {
        Map<String, Object> query = queryString != null ? jsonParser.parseMap(queryString) : null;
        return service.getPage(page, pageSize, sortField, sortOrder, query);
    }

    protected Resource get(ID id, String jwtToken) {
       return   service.findOptionalById(id).orElseThrow(ResourceNotFoundException::new);

    }

    protected ResponseEntity<Void> update(ID id, Resource resource, String jwtToken) {
        service.update(id, resource);
        return buildUpdateResponse();
    }

    protected ResponseEntity<Resource> create(Resource resource, String jwtToken) {
        Resource persistedResource = service.create(resource);
        return buildCreateResponse(persistedResource);
    }

    protected ResponseEntity<Void> delete(ID id, String jwtToken) {
        service.delete(id);
        return buildDeleteResponse();
    }

    protected ResponseEntity<Void> buildUpdateResponse() {
        return ResponseEntity.noContent().build();
    }

    protected ResponseEntity<Resource> buildCreateResponse(Resource persistedResource) {
        return new ResponseEntity<Resource>(persistedResource, HttpStatus.CREATED);
    }

    protected ResponseEntity<Void> buildDeleteResponse() {
        return ResponseEntity.noContent().build();
    }



    public static class ResourceNotFoundException extends RuntimeException {
    }
}
