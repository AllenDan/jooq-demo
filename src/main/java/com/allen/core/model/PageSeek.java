package com.allen.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Author: sush
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageSeek<T> {

    private List<T> list;

    private int totalRow;
    private int totalPage;
    private int pageSize;
    private Object next;

}
