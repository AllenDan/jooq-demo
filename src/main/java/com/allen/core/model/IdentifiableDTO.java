package com.allen.core.model;

import java.io.Serializable;

/**
 * author: Allen
 */
public interface IdentifiableDTO<T> extends Serializable {

    T getId();


}

