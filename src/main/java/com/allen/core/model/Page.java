package com.allen.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Author: sush
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Page<T> implements Serializable {
    private static final long serialVersionUID = 8876752589804984100L;

    private List<T> list;
    private int pageNum;
    private int pageSize;
    private int totalPage;
    private int totalRow;

}
