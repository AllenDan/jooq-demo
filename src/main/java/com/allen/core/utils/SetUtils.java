package com.allen.core.utils;

import com.google.common.collect.Sets;

import java.util.Set;

public class SetUtils {

    /**
     * 两个集合取交集，返回两个集合且移除交集的元素
     * example:
     *  集合1：1，2，3
     *  集合2：3，4，5
     *  return
     *  集合1：1，2
     *  集合2：4，5
     * @param newSets
     * @param oldSets
     * @return
     */
    public static SetTuple<Set<Long>,Set<Long>> calculate(Set<Long> newSets , Set<Long> oldSets){
        // Empty Set
        Set<Long> middle = Sets.newHashSet();
        middle.addAll(oldSets);
        middle.retainAll(newSets);
        newSets.removeAll(middle);
        oldSets.removeAll(middle);
        return new SetTuple<>(newSets,oldSets);
    }


}




