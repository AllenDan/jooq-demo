package com.allen.core.utils;

import lombok.Getter;

public class SetTuple<N,O> {
    @Getter
    private N newElement;
    @Getter
    private O oldElement;

    public SetTuple(N newElement,O oldElement){
        this.newElement = newElement;
        this.oldElement = oldElement;
    }
}
