package com.allen.user.util;

public interface BaseResult<E> extends BaseError {

	
	/**
	 * 
	 * @return 成功时返回数据
	 */
	E getData();
}
