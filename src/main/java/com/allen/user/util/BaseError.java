package com.allen.user.util;

public interface BaseError {
	
	/**
	 * 错误代码
	 * 
	 * @return
	 */
	String getCode();

	/**
	 * 错误信息
	 * 
	 * @return
	 */
	String getErrorMsg();

	/**
	 * 详细说明
	 * 
	 * @return
	 */
	String getDetail();

	/**
	 * 取得枚举的名字
	 * 
	 * @return
	 */
	String getName();
	
}
