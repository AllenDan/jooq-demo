package com.allen.user.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description: 分页信息
 * @author: danxiaoqiang
 * @create: 2019-06-06 11:27
 **/
@Data
@ApiModel(description="分页数据")
public class PageList<T>  implements Serializable{
    private static final long serialVersionUID = -1709150908478892562L;
    @ApiModelProperty(value = "当前页面记录数量")
    private int pageSize;
    @ApiModelProperty(value = "当前页码")
    private int page;
    @ApiModelProperty(value = "总记录数")
    protected int totalRecord;
    @ApiModelProperty(value = "总页数")
    protected int pageCount;
    @ApiModelProperty(value = "分页数据")
    protected List<T> results;
}
