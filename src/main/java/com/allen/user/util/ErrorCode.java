package com.allen.user.util;

public enum ErrorCode {

	GENERIC_SUCCESS("0"),
	
	SPECIFIC_SUCCESS("0"),
	
	/**
	 * 参数错误
	 */
	PARAM_ERROR("-400"), // 参数错误

	/**
	 * 业务流程异常
	 */
	BUSINESS_ERROR("500") ;// 业务流程异常


	String code;

	private ErrorCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	@Override
	public String toString() {
		return code;
	}

	public boolean equalsCode(String code) {
		return this.code.equals(code);
	}
}
