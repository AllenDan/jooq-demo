package com.allen.user.util;

import lombok.Data;

@Data
public class BusinessError implements BaseError {
	
	private String name;
	private String code;
	private String errorMsg;
	private String detail;
	
	public BusinessError(BusinessCode bc) {
		this(bc, ErrorCode.GENERIC_SUCCESS, null, null);
	}
	
	public BusinessError(BusinessCode bc, ErrorCode ec, String message) {
		this(bc, ec, message, null);
	}
	
	public BusinessError(BusinessCode bc, ErrorCode ec, String errorMsg, String detail) {
		this.name = bc.getName();
		this.code = ec.getCode() + bc.getCode();
		this.errorMsg = errorMsg;
		this.detail = detail;
	}

	
}
