package com.allen.user.util;

public interface BusinessCode {

	 String getName();
	 String getCode();
	
}
