package com.allen.user.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @author Zhu
 * 
 */
@Data
@ApiModel(description="接口请求结果")
public class Result<E> implements BaseResult<E>, Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty(value = "返回的状态码，0-成功",name="name",required = true)
	private String code;
	@ApiModelProperty(hidden = true)

	private String name;
	@ApiModelProperty(value = "请求失败返回的异常提醒")
	private String errorMsg;
	@ApiModelProperty(hidden = true)
	private String detail;
	@ApiModelProperty(value = "请求成功返回的数据")
	private E data;



	public boolean ok() {
		if (code == null || code.length() == 0) {
			return false;
		}
		if (code.equals(ErrorCode.GENERIC_SUCCESS.getCode())) {
			return true;
		}
		if (code.startsWith(ErrorCode.SPECIFIC_SUCCESS.getCode())) {
			return true;
		}
		return false;
	}

	private Result() {
	}

	private Result(BaseError b) {
		setCode(b.getCode());
		setName(b.getName());
		setErrorMsg(b.getErrorMsg());
		setDetail(b.getDetail());
	}

	public static <E> Result<E> success() {
		return success((E) null);
	}

	public static <E> Result<E> success(E data) {
		Result<E> r = new Result<>();
		r.setCode(ErrorCode.GENERIC_SUCCESS.getCode());
		r.setData(data);
		return r;
	}

	public static <E> Result<E> success(String code, E data) {
		Result<E> r = new Result<>();
		r.setCode(code);
		r.setData(data);
		return r;
	}

	public static <E> Result<E> fail(String code, String errorMsg) {
		return fail(code, errorMsg, null);
	}

	public static <E> Result<E> fail(String code, String errorMsg, String detail) {
		Result<E> r = new Result<>();
		r.setCode(code);
		r.setErrorMsg(errorMsg);
		r.setDetail(detail);
		return r;
	}

	public static <E> Result<E> fail(BaseError b) {
		return new Result<E>(b);
	}

}
