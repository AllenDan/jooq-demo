package com.allen.user.repository;




import com.allen.user.dto.GroupDTO;
import org.jooq.DSLContext;
import org.springframework.stereotype.Component;

import static com.allen.jooq.tables.Group.GROUP;

@Component
public class GroupRepository {
    private final DSLContext dslContext;

    public GroupRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }
    public Integer addGroup(GroupDTO groupDTO){
        return dslContext.insertInto(GROUP).columns(GROUP.NAME).values(groupDTO.getName()).execute();
    }
}
