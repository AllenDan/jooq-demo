package com.allen.user.repository;





import com.allen.user.dto.UserDTO;
import org.jooq.Condition;
import org.jooq.DSLContext;

import org.jooq.SortOrder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.allen.jooq.tables.Group.GROUP;
import static com.allen.jooq.tables.GroupMember.GROUP_MEMBER;
import static com.allen.jooq.tables.User.USER;
import static org.jooq.impl.DSL.trueCondition;


@Component
public class UserRepository  {
    private final DSLContext dslContext;
    public UserRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }
    public Integer addUser(UserDTO userDTO){
        return dslContext.insertInto(USER).columns(USER.NAME,USER.LOGIN,USER.MOBILE,USER.PASSWORD)
                .values(userDTO.getName(),userDTO.getLogin(),userDTO.getMobile(),userDTO.getPassword()).execute();
    }

    public Integer updateUser(UserDTO userDTO){
        return dslContext.update(USER).set(USER.NAME,userDTO.getName())
                .set(USER.LOGIN,userDTO.getLogin())
                .set(USER.MOBILE,userDTO.getMobile())
                .set(USER.PASSWORD,userDTO.getPassword())
                .where(USER.ID.eq(userDTO.getId()))
                .execute();
    }

    public List<UserDTO> selectUsers(Map<String, Object> filterQuery){
        return dslContext.selectFrom(USER)
                  .where(filter(filterQuery != null ? filterQuery : new HashMap<>()))
                 .fetchInto(UserDTO.class);

    }
    public Integer countUsers(Map<String, Object> filterQuery){
        return dslContext.selectCount().from(USER)
                .where(filter(filterQuery != null ? filterQuery : new HashMap<>()))
                .fetchOneInto(Integer.class);

    }
    public List<UserDTO> selectPageUsers(Integer page, Integer pageSize, String sortField, String sortOrder,Map<String, Object> filterQuery){
        return dslContext.selectFrom(USER)
                .where(filter(filterQuery != null ? filterQuery : new HashMap<>()))
                .orderBy(USER.field(sortField).sort(SortOrder.valueOf(sortOrder)))
                .limit(pageSize)
                .offset((page - 1) * pageSize)
                .fetchInto(UserDTO.class);
    }

    public List<UserDTO> selectPageUserJoinMsg(Integer page, Integer pageSize, String sortField, String sortOrder,Map<String, Object> filterQuery){
        return dslContext.select(USER.ID,USER.LOGIN,USER.MOBILE,USER.NAME,USER.CREATED_AT,USER.ENABLED,GROUP.NAME.as("groupName"))
                .from(USER)
                .leftJoin(GROUP_MEMBER).on(USER.ID.eq(GROUP_MEMBER.USER_ID))
                .leftJoin(GROUP).on(GROUP_MEMBER.GROUP_ID.eq(GROUP.ID))
                .where(filter(filterQuery != null ? filterQuery : new HashMap<>()))
                .orderBy(USER.field(sortField).sort(SortOrder.valueOf(sortOrder)))
                .limit(pageSize)
                .offset((page - 1) * pageSize)
                .fetchInto(UserDTO.class);
    }

    protected Condition filter(Map<String, Object> filterQuery) {
        Condition condition = trueCondition();
        if(filterQuery.get("login")!=null){
            condition.and(USER.LOGIN.eq(filterQuery.get("login").toString().trim()));
        }
        if(filterQuery.get("name")!=null){
            condition.and(USER.LOGIN.eq(filterQuery.get("name").toString().trim()));
        }
        if(filterQuery.get("mobile")!=null){
            condition.and(USER.LOGIN.eq(filterQuery.get("mobile").toString().trim()));
        }
        return condition;

    }





    }
