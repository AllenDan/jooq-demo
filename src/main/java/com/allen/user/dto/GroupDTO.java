package com.allen.user.dto;


import com.allen.jooq.tables.pojos.Group;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @description: 权限组内容
 * @author: danxiaoqiang
 * @create: 2019-06-05 18:15
 **/
@ApiModel(description="权限组对象")
@Data
public class GroupDTO  {
    @ApiModelProperty(value = "名称,不为空",name="name",required = true)
    private String name;
    @ApiModelProperty(value = "权限id",dataType = "Long")
    private Long id;
    @ApiModelProperty(value = "创建时间",dataType = "java.util.Date",example = "2018-07-12 11:12:30")
    private Timestamp createdAt;
    @ApiModelProperty(value="创建时间",hidden=true)
    private Timestamp updatedAt;
}
