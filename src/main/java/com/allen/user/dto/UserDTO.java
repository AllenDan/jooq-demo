package com.allen.user.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class UserDTO  {
    private Long      id;
    @ApiModelProperty(value = "用户账号",required = true)
    private String    login;
    @ApiModelProperty(value = "用户密码",required = true)
    private String    password;
    @ApiModelProperty(value = "用户名称",required = true)
    private String    name;
    @ApiModelProperty(value = "用户手机号",required = true)
    private String    mobile;
    @ApiModelProperty(value = "是否可用",required = true,dataType = "Boolean")
    private Boolean   enabled;
    @ApiModelProperty(value = "创建时间",hidden=true)
    private Timestamp createdAt;
    @ApiModelProperty(value="创建时间",hidden=true)
    private Timestamp updatedAt;
    @ApiModelProperty(value="所在权限组",hidden=true)
    private String groupName;
}
