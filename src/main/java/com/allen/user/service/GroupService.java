package com.allen.user.service;


import com.allen.jooq.tables.Group;
import com.allen.user.dto.GroupDTO;
import com.allen.user.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GroupService {
    @Autowired
    private GroupRepository groupRepository;

    public boolean addGroup(GroupDTO groupDTO){
        if(groupRepository.addGroup(groupDTO)>0){
            return true;
        }
        return false;
    }
}
