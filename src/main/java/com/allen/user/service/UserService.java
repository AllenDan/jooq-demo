package com.allen.user.service;

import com.allen.user.dto.UserDTO;
import com.allen.user.repository.UserRepository;
import com.allen.user.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserService  {
    @Autowired
    private UserRepository userRepository;
    public boolean addUser(UserDTO userDTO){
        if(userRepository.addUser(userDTO)>0){
            return true;
        }
        return false;
    }
    public boolean updateUser(UserDTO userDTO){
        if(userRepository.updateUser(userDTO)>0){
            return true;
        }
        return false;
    }
    public List<UserDTO> getUserDTO(Map<String, Object> filterQuery){
        return userRepository.selectUsers(filterQuery);
    }
    public PageList<UserDTO> pageUsers(Integer page, Integer pageSize, String sortField, String sortOrder,Map<String, Object> filterQuery){
        PageList pageList = new PageList();
        pageList.setPage(page);
        pageList.setPageSize(pageSize);
        int total = userRepository.countUsers(filterQuery);
        pageList.setTotalRecord(total);
        pageList.setPageCount(total%pageSize==0?total/pageSize:total/pageSize+1);
        pageList.setResults(userRepository.selectPageUsers( page,  pageSize, sortField,  sortOrder,filterQuery));
        return pageList;
    }
}
