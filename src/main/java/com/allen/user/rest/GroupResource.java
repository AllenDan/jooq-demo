package com.allen.user.rest;

import com.allen.user.dto.GroupDTO;
import com.allen.user.service.GroupService;
import com.allen.user.util.ErrorCode;
import com.allen.user.util.Result;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/group")
@Api(description = "权限组相关接口")
public class GroupResource {
    @Autowired
    private GroupService groupService;
    @PostMapping(value="/add")
    @ApiOperation("添加权限组")
    public Result<String> addOneGroup(@RequestBody @ApiParam(value="传入json格式,参数name不为空",required = true)
                                                  GroupDTO groupVO){
        if(groupService.addGroup(groupVO)){
            return Result.success("SUCCESS");
        }
        return Result.fail(ErrorCode.BUSINESS_ERROR.getCode(),"添加权限组失败");
    }
}
