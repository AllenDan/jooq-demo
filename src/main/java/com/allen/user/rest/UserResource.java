package com.allen.user.rest;





import com.alibaba.fastjson.JSONObject;
import com.allen.user.dto.UserDTO;
import com.allen.user.service.UserService;
import com.allen.user.util.ErrorCode;
import com.allen.user.util.PageList;
import com.allen.user.util.Result;
import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;



import java.util.Map;

@RestController
@RequestMapping("/user")
@Api(description = "用户相关查询接口")
public class UserResource  {
    @Autowired
    private UserService userService;

    @PostMapping(value="/add")
    @ApiOperation("添加用户")
    public Result<String> addUser(@RequestBody @ApiParam(value="传入json格式",required = true)
                                              UserDTO userDTO){
        if(userService.addUser(userDTO)){
            return Result.success("SUCCESS");
        }
        return Result.fail(ErrorCode.BUSINESS_ERROR.getCode(),"添加用户失败");
    }

    @PostMapping(value="/update")
    @ApiOperation("更新用户信息")
    public Result<String> updateUser(@RequestBody @ApiParam(value="传入json格式,参数name、mobile、login、password不为空",required = true)
                                              UserDTO userDTO){
        if(userService.addUser(userDTO)){
            return Result.success("SUCCESS");
        }
        return Result.fail(ErrorCode.BUSINESS_ERROR.getCode(),"更新用户信息");
    }

    @GetMapping(value="/page-data")
    @ApiOperation("查询分页的用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name="filterQuery",paramType ="query",
                    value = "请求条件参数，json字符串",example ="{'login':'allen','mobile':'15220159764','name':'allen'}" )
    })
    public Result<PageList<UserDTO>> pageUser(@RequestParam @ApiParam(value = "当前页",defaultValue = "1") Integer page,
                                              @RequestParam @ApiParam(value = "当前页的数量",defaultValue = "5")Integer pageSize,
                                              @RequestParam @ApiParam(value = "排序字段名称",defaultValue = "id")String sortField,
                                              @RequestParam @ApiParam(value = "升降序排列，DESC-降序，ASC-升序",defaultValue = "DESC")String sortOrder,
                                                String filterQuery){
        Map<String,Object> param=null;
        if(!StringUtils.isEmpty(filterQuery)){
             param = JSONObject.parseObject(filterQuery,Map.class);
        }
        return Result.success(userService.pageUsers(page,pageSize,sortField,sortOrder,param));
    }


}
