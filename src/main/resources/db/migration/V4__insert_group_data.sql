/** password: 888888 **/

INSERT INTO `group` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1, '超级管理员', '2017-08-07 00:00:00', '2017-08-07 00:00:00');

INSERT INTO `group_member` (`id`, `group_id`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1, 1, 60001, '2017-08-07 00:00:00', '2017-08-07 00:00:00');


INSERT INTO `group_authority` (`id`, `group_id`, `authority`, `created_at`, `updated_at`)
VALUES
	(1, 1, 'ROLE_ADMIN', '2017-08-07 00:00:00', '2017-08-07 00:00:00');
